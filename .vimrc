" =========== config ===============

syntax on
filetype plugin indent on

set nocompatible
set modelines=0

set number
set nowrap
set linebreak
set formatoptions=tcrqj " just format comments

set guioptions=aAet


if has("gui_running")
  set lines=40
  set columns=150
  set guifont=Source\ Code\ Pro\ for\ Powerline\ 12
  " set guifont=Source\ Code\ Pro\ for\ Powerline:h13
endif

set linespace=5

set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set shiftround  " when at 3 spaces, and I hit > ... go to 4, not 7d

" autocmd FileType coffee,stylus setlocal tabstop=2 shiftwidth=2 softtabstop=2

set foldmethod=indent
set foldlevel=100

set nobackup
set nowritebackup
set noswapfile

set encoding=utf-8
set autoindent
set showmode
set showcmd
set hidden
set confirm  " confirm question on command with unsaved file
set wildmenu  " auto completion for command line!!!
set wildmode=list:longest
set visualbell
set cursorline
set ttyfast
set ruler
set backspace=indent,eol,start
set laststatus=2
set statusline=%f%(\ [%M%R%W]%)\ %Y\ %=\ %vx%l/%L\ [%p%%]

set autoread  " automatically read file changed outside of Vim
set history=1000
au FocusLost * :wa  " save on every lost focus!
set autowriteall

set complete+=U  " adds buffer not bufferlist to completion-source
set completeopt-=preview
autocmd FileType css,less,stylus setlocal omnifunc=csscomplete#CompleteCSS
autocmd FileType html,markdown,handlebars setlocal omnifunc=htmlcomplete#CompleteTags
autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
" sets omnicomplete to syntax if no omni-function is set
if has("autocmd") && exists("+omnifunc")
  autocmd Filetype *
        \	if &omnifunc == "" |
        \		setlocal omnifunc=syntaxcomplete#Complete |
        \	endif
endif

set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch

set wildignore+=*/.git/*,*.class,*/.idea/*,*/.hg/*

" cljx filetype
autocmd BufNewFile,BufReadPost *.cljx setfiletype clojure

"""""""""""""""""""""" mapings """""""""""""""""""""

let mapleader = "m"
let maplocalleader = ","

" auto completion
inoremap <c-f> <c-x><c-f>
inoremap <c-l> <c-x><c-l>
" inoremap <c-space> <c-x><c-o>

" indentation
vmap <s-tab> <
vmap <tab> >

" moving
nmap <cr> <c-d>
nmap <backspace> <c-u>
nmap <c-cr> <c-f>
nmap <c-backspace> <c-b>
nmap L $
vmap L $
nmap H ^
vmap H ^ 
nmap - $
vmap - $
nmap j gj
nmap k gk
vmap j gj
vmap k gk
nmap <left> zh
nmap <right> zl
nmap <s-left> zH
nmap <s-right> zL
nmap + `
nmap ++ ``

nnoremap <leader><space> :noh<cr>
imap jj <esc>
imap kk <esc>
nmap n nzz
nmap N Nzz

" search for visual selection
vnorem // y/<c-r>"<cr>

" window navigation
nmap <tab> <c-w>w
nmap <s-tab> <c-w>W
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l
nmap <leader>t :tabnew<cr>
nmap <leader>T :tabclose<cr>

" open and load this config file
nmap <leader>,o :e ~/.vimrc<cr>
nmap <leader>,l :w<cr>:so ~/.vimrc<cr>

nmap <leader>o o<esc>k
nmap <leader>O O<esc>j

nmap <c-s> :w<cr>
imap <c-s> <esc>:w<cr>

autocmd CmdwinEnter * nmap <buffer> <cr> i<cr>

imap ö (
imap ä [
imap ü {
imap Ö )
imap Ä ]
imap Ü }
