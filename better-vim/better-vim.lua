return {
  mappings = {
    leader = "m",
    custom = {
      ["<leader>f"] = { name = "+file" },
      ["<leader>ff"] = { "<cmd>Telescope find_files<cr>", "Find File" },
      ["<leader>fr"] = { "<cmd>Telescope oldfiles<cr>", "Open Recent File" },
      ["<leader>fg"] = { "<cmd>Telescope buffers<cr>", "Open Recent File" },
      ["<leader><space>"] = { "<cmd>nohl<cr>", "nohl" },
      ["<ctrl><backspace>"] = { "<cmd>NvimTreeFindFile<cr>", "Focus file in Tree" },
      ["-"] = { "$", "End of line" },
      ["L"] = { "$", "End of line" },
      ["H"] = { "^", "Start of line" },
      ["<c-h>"] = { "<cmd>bprevious<cr>", "Next buffer" },
      ["<c-l>"] = { "<cmd>bnext<cr>", "Next buffer" },
    },
    by_mode = {
      i = {
        ["jj"] = {"<ESC>", "Exit insert mode"},
        ["kk"] = {"<ESC>", "Exit insert mode"},

        ["ö"] = {"(", "(", noremap=false },
        ["ä"] = {"[", "[", noremap=false },
        ["ü"] = {"{", "{", noremap=false },
        ["Ö"] = {")", ")", noremap=false },
        ["Ä"] = {"]", "]", noremap=false },
        ["Ü"] = {"}", "}", noremap=false },
        ["<c-o>"] = {"ö", "ö"},
        ["<c-a>"] = {"ä", "ä"},
        ["<c-u>"] = {"ü", "ü"},
      }
    }
  },
}
