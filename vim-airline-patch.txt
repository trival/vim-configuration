" use this to set horizontal window border colors of airline to same as vertical
let s:IA = airline#themes#get_highlight2(['NonText', 'fg'], ['VertSplit', 'bg'])

