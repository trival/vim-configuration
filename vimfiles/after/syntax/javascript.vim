syntax match   jsFunctionProp    /\<.[a-zA-Z_$][0-9a-zA-Z_$]*\(\s*=\s*function\s*\)\@=/

" A constant-like name in SCREAMING_CAPS
syntax match jsMyConstant /\<\u[A-Z0-9_]\+\>/ display
syntax match jsMyContainedConstant /\<\u[A-Z0-9_]\+\>/ display contained
syntax cluster jsAll contains=@jsExpression,jsLabel,jsConditional,jsRepeat,jsReturn,jsStatement,jsTernaryIf,jsException,jsMyConstant,jsMyContainedConstant
hi def link jsMyConstant Constant
hi def link jsMyContainedConstant Constant
