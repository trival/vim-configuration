syntax keyword coffeeGlobalObjects  Array Boolean Date Function Iterator Number Object RegExp String Proxy ParallelArray ArrayBuffer DataView Float32Array Float64Array Int16Array Int32Array Int8Array Uint16Array Uint32Array Uint8Array Uint8ClampedArray Intl JSON Math console document window
syntax match   coffeeGlobalObjects  /\%(Intl\.\)\@<=\(Collator\|DateTimeFormat\|NumberFormat\)/
syntax keyword coffeeGlobalObjects  Error EvalError InternalError RangeError ReferenceError StopIteration SyntaxError TypeError URIError
syntax keyword coffeeGlobalObjects  decodeURI decodeURIComponent encodeURI encodeURIComponent eval isFinite isNaN parseFloat parseInt uneval
syntax keyword coffeeGlobalObjects  DOMImplementation DocumentFragment Document Node NodeList NamedNodeMap CharacterData Attr Element Text Comment CDATASection DocumentType Notation Entity EntityReference ProcessingInstruction
syntax keyword coffeeGlobalObjects  DOMException

syn match coffeeDotAccess /NONESENSEWORD/
syn match coffeeProtoAccess /NONESENSEWORD/
syn match coffeeObjAssign /NONESENSEWORD/
syn match coffeeParens /[()]/
hi def link coffeeParens Noise
syn match coffeeFuncInvokation /\%([0-9a-zA-Z_$@]\>\)\@<=()/
hi def link coffeeFuncInvokation Operator
syn match coffeeFuncNameLineStart /\%(^\<[a-zA-Z_$@][0-9a-zA-Z_$]*\ze\s*[=:]\s*\)\=\%((\([0-9a-zA-Z_$,\{\}\[\]:=]\|\s\)*)\)\=\s*[-=]>/
hi def link coffeeFuncNameLineStart Identifier
syn match coffeeFuncName /\%(\%(\s\|\.\)\<[a-zA-Z_$@][0-9a-zA-Z_$]*\ze\s*[=:]\s*\)\=\%((\([0-9a-zA-Z_$,\{\}\[\]:=]\|\s\)*)\)\=\s*[-=]>/hs=s+1
hi def link coffeeFuncName Identifier
syn match coffeeFuncStart /(\ze\([0-9a-zA-Z_$@,\{\}\[\]:=]\|\s\)*)\s*[-=]>/
hi def link coffeeFuncStart Operator
syn match coffeeFuncEnd /)\?\s*[-=]>/
hi def link coffeeFuncEnd Operator
syn cluster coffeeAll contains=coffeeStatement,coffeeRepeat,coffeeConditional,
\                              coffeeException,coffeeKeyword,coffeeOperator,
\                              coffeeExtendedOp,coffeeSpecialOp,coffeeBoolean,
\                              coffeeGlobal,coffeeSpecialVar,coffeeSpecialIdent,
\                              coffeeObject,coffeeConstant,coffeeString,
\                              coffeeNumber,coffeeFloat,coffeeReservedError,
\                              coffeeObjAssign,coffeeComment,coffeeBlockComment,
\                              coffeeEmbed,coffeeRegex,coffeeHeregex,
\                              coffeeHeredoc,coffeeSpaceError,
\                              coffeeSemicolonError,coffeeDotAccess,
\                              coffeeProtoAccess,coffeeCurlies,coffeeBrackets,
\                              coffeeParens, coffeeFuncInvokation, coffeeFuncName,
\                              coffeeFuncStart, coffeeFuncEnd
hi link coffeeSpaceError NONE
